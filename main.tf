terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.61.0"
    }
  }
}

provider "aws" {
  region = "eu-west-2"
}


# TLS cert
resource "aws_acm_certificate" "fotd" {
  domain_name               = "fotd.markdurham.co.uk"
  validation_method         = "DNS"
  subject_alternative_names = ["api.fotd.markdurham.co.uk"]

  tags = var.tags
}

# Route 53 domain validation records
resource "aws_route53_record" "fotd_dvo" {
  for_each = {
    for dvo in aws_acm_certificate.fotd.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  ttl     = 60
  name    = each.value.name
  records = [each.value.record]
  type    = each.value.type
  zone_id = var.markdurham_co_uk_zone
}

# TLS cert validation
resource "aws_acm_certificate_validation" "fotd" {
  certificate_arn         = aws_acm_certificate.fotd.arn
  validation_record_fqdns = [for record in aws_route53_record.fotd_dvo : record.fqdn]
}

# Route 53 records
resource "aws_route53_record" "fotd" {
  zone_id = var.markdurham_co_uk_zone
  name    = "fotd.markdurham.co.uk"
  type    = "A"

  alias {
    name                   = aws_lb.fotd.dns_name
    zone_id                = aws_lb.fotd.zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "api" {
  zone_id = var.markdurham_co_uk_zone
  name    = "api.fotd.markdurham.co.uk"
  type    = "A"

  alias {
    name                   = aws_lb.fotd.dns_name
    zone_id                = aws_lb.fotd.zone_id
    evaluate_target_health = false
  }
}

# VPC
resource "aws_vpc" "fotd" {
  cidr_block = "192.168.0.0/16"

  tags = var.tags
}

# Subnets
resource "aws_subnet" "lb_a" {
  vpc_id                  = aws_vpc.fotd.id
  cidr_block              = "192.168.1.0/24"
  availability_zone       = "eu-west-2a"
  map_public_ip_on_launch = true

  tags = var.tags
}

resource "aws_subnet" "lb_b" {
  vpc_id                  = aws_vpc.fotd.id
  cidr_block              = "192.168.2.0/24"
  availability_zone       = "eu-west-2b"
  map_public_ip_on_launch = true

  tags = var.tags
}

resource "aws_subnet" "lb_c" {
  vpc_id                  = aws_vpc.fotd.id
  cidr_block              = "192.168.3.0/24"
  availability_zone       = "eu-west-2c"
  map_public_ip_on_launch = true

  tags = var.tags
}

resource "aws_subnet" "web_a" {
  vpc_id            = aws_vpc.fotd.id
  cidr_block        = "192.168.11.0/24"
  availability_zone = "eu-west-2a"

  tags = var.tags
}

resource "aws_subnet" "web_b" {
  vpc_id            = aws_vpc.fotd.id
  cidr_block        = "192.168.12.0/24"
  availability_zone = "eu-west-2b"

  tags = var.tags
}

resource "aws_subnet" "web_c" {
  vpc_id            = aws_vpc.fotd.id
  cidr_block        = "192.168.13.0/24"
  availability_zone = "eu-west-2c"

  tags = var.tags
}

resource "aws_subnet" "db_a" {
  vpc_id            = aws_vpc.fotd.id
  cidr_block        = "192.168.21.0/24"
  availability_zone = "eu-west-2a"

  tags = var.tags
}

resource "aws_subnet" "db_b" {
  vpc_id            = aws_vpc.fotd.id
  cidr_block        = "192.168.22.0/24"
  availability_zone = "eu-west-2b"

  tags = var.tags
}

resource "aws_subnet" "db_c" {
  vpc_id            = aws_vpc.fotd.id
  cidr_block        = "192.168.23.0/24"
  availability_zone = "eu-west-2c"

  tags = var.tags
}

# Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.fotd.id

  tags = var.tags
}

# Route table
resource "aws_route_table" "lb" {
  vpc_id = aws_vpc.fotd.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = var.tags
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.lb_a.id
  route_table_id = aws_route_table.lb.id
}

resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.lb_b.id
  route_table_id = aws_route_table.lb.id
}

resource "aws_route_table_association" "c" {
  subnet_id      = aws_subnet.lb_c.id
  route_table_id = aws_route_table.lb.id
}

# Security groups
resource "aws_security_group" "web" {
  vpc_id = aws_vpc.fotd.id
  name   = "fotd-web-sg"

  ingress {
    description     = "Allow HTP requests from ALB SG"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.lb.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

resource "aws_security_group" "lb" {
  vpc_id = aws_vpc.fotd.id
  name   = "fotd-lb-sg"

  ingress {
    description = "Allow HTTP requests from anywhere"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow HTTPS requests from anywhere"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}

resource "aws_security_group" "db" {
  vpc_id = aws_vpc.fotd.id
  name   = "fotd-db-sg"

  ingress {
    description     = "Allow DB requests from web SG"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.web.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.tags
}


# RDS Subnet group
resource "aws_db_subnet_group" "fotd" {
  subnet_ids = [aws_subnet.db_a.id, aws_subnet.db_b.id, aws_subnet.db_c.id]

  tags = var.tags
}

# RDS Instance
resource "aws_db_instance" "fotd_db" {
  identifier             = "fotd"
  vpc_security_group_ids = [aws_security_group.db.id]
  snapshot_identifier    = var.rds_snapshot
  instance_class         = "db.t3.micro"
  db_subnet_group_name   = aws_db_subnet_group.fotd.name
  skip_final_snapshot    = true

  tags = var.tags
}


# Assume role policy
data "aws_iam_policy_document" "ec2_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

# Role
resource "aws_iam_role" "fotd_app" {
  name                = "Fotd-App"
  path                = "/"
  assume_role_policy  = data.aws_iam_policy_document.ec2_assume_role.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"]

  tags = var.tags
}

# Instance profile
resource "aws_iam_instance_profile" "fotd_app" {
  name = "fotd-app"
  role = aws_iam_role.fotd_app.name

  tags = var.tags
}

# Launch template
resource "aws_launch_template" "fotd" {
  name                   = "fotd"
  instance_type          = "t2.micro"
  image_id               = var.ami_id
  vpc_security_group_ids = [aws_security_group.web.id]
  key_name               = "mark-kp"
  iam_instance_profile {
    arn = aws_iam_instance_profile.fotd_app.arn
  }

  tags = var.tags

  depends_on = [aws_db_instance.fotd_db]
}

# ASG
resource "aws_autoscaling_group" "fotd" {
  name             = "asg-fotd"
  min_size         = 1
  max_size         = 2
  desired_capacity = 1
  launch_template {
    id      = aws_launch_template.fotd.id
    version = "$Latest"
  }
  vpc_zone_identifier = [aws_subnet.web_a.id, aws_subnet.web_b.id, aws_subnet.web_c.id]
  target_group_arns   = [aws_lb_target_group.asg_fotd.arn]

  tag {
    key                 = "Provider"
    value               = "ASG"
    propagate_at_launch = true
  }

  tag {
    key                 = "app"
    value               = "FotD"
    propagate_at_launch = true
  }
}

# Target group
resource "aws_lb_target_group" "asg_fotd" {
  name     = "asg-fotd"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.fotd.id

  tags = var.tags
}

# ALB
resource "aws_lb" "fotd" {
  name               = "lb-fotd"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb.id]
  subnets            = [aws_subnet.lb_a.id, aws_subnet.lb_b.id, aws_subnet.lb_c.id]

  tags = var.tags
}

# ALB listeners
resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.fotd.id
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS13-1-2-2021-06"
  certificate_arn   = aws_acm_certificate_validation.fotd.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg_fotd.arn
  }

  tags = var.tags
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.fotd.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  tags = var.tags
}

# ASG ALB Attachment
resource "aws_autoscaling_attachment" "asg_fotd" {
  autoscaling_group_name = aws_autoscaling_group.fotd.id
  lb_target_group_arn    = aws_lb_target_group.asg_fotd.arn
}

